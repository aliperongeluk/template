const swaggerUi = require('swagger-ui-express');
const swaggerEnv = require('./src/environment/swagger');
const mongo = require('./src/environment/db/mongo');
const amqp = require('./src/receiver.rabbitmq');

const express = require('express');
const app = express();

const templateRoutes = require('./src/routes/template.routes');
const rabbitmq = require('./src/routes/template.rabbitmq.routes');

const bodyParser = require('body-parser');

const { connect } = require('./src/environment/rabbitmq-connection');

const ip = require('ip')
const Eureka = require('eureka-js-client').Eureka;

// body-parser:
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(
  bodyParser.json({
    type: 'application/vnd.api+json',
  })
);

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/template', templateRoutes);
app.use('/rabbit-mq', rabbitmq.routes);

// default:
app.use('*', (req, res) => {
  res.status(400).send({
    error: 'not available',
  });
});

// connect to rabbit-mq server:
connect()
  .then(conn => {
    rabbitmq.setRabbitMQConnection(conn);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

const eurekaServer = process.env.EUREKA_SERVER || 'eurekaserver-1';
const eurekaServerPort = process.env.EUREKA_PORT || 8761;
const ipAddr = process.env.IP_ADDRESS || ip.address();
const hostName = process.env.HOST_NAME || ip.address();
const port = process.env.PORT || 6969;

const eurekaClient = new Eureka({
  // application instance information
  instance: {
      app: 'template',
      hostName: hostName,
      ipAddr: ipAddr,
      homePagekUrl: `http://${ipAddr}:${port}`,
      port: {
          '$': port,
          '@enabled': 'true',
      },
      vipAddress: 'template',
      dataCenterInfo: {
          '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
          name: 'MyOwn',
      }
  },
  eureka: {
      // eureka server host / port
      host: eurekaServer,
      port: eurekaServerPort,
      servicePath: '/eureka/apps/',
      maxRetries: 10,
      registerWithEureka: true,
      fetchRegistry: true
  }
});

eurekaClient.start();

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server running on port [${port}]`);
});
