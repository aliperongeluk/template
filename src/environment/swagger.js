const swaggerJsDoc = require('swagger-jsdoc');

const options = {
  definition: {
    swagger: '2.0',
    info: {
      title: 'Template API Docs',
      version: '1.0',
      description: 'Template to test express api with swagger documentation',
    },
  },

  apis: ['./src/routes/*.js'],
};

const specs = swaggerJsDoc(options);

module.exports = specs;
