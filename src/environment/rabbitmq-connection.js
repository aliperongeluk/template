// LOCAL: amqp://localhost
// SERVER: amqp://AliB:perongelukexpres@rabbitmq

const amqp = require('amqplib');
let counter = 0;

const connect = () => {
  return new Promise((resolve, reject) => {
    amqp
      .connect('amqp://AliB:perongelukexpres@rabbitmq')
      .then(connection => {
        counter = 0;
        resolve(connection);
      })
      .catch(error => {
        if (counter == 10) {
          reject(error);
        }

        setTimeout(() => {
          counter++;
          connect();
        }, 3000);
      });
  });
};

module.exports = { connect };
