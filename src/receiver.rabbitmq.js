const amqp = require('amqplib');

amqp
  .connect('amqp://AliB:perongelukexpres@rabbitmq')
  .then(conn => {
    conn
      .createChannel()
      .then(ch => {
        ch.assertQueue('quetest', { durable: true });
        ch.consume(
          'quetest',
          function(msg) {
            console.log(msg);
            console.log(msg.content.toString('utf-8'));
          },
          {
            noAck: true,
          }
        );
      })
      .catch(err => {
        console.log(err);
      });
  })
  .catch(err => {
    console.log(err);
  });

module.exports = amqp;
