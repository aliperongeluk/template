const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TemplateSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'This name is required.'],
    },
    number: {
      type: Number,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

const Template = mongoose.model('template', TemplateSchema);

module.exports = Template;
