const express = require('express');
const routes = express.Router();
let rabbitmqConnection;

/**
 * @swagger
 * /rabbit-mq:
 *    post:
 *     tags:
 *       - template (microservice)
 *       - new tag
 *     description: This does something with rabbitmq
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: something sends to rabbitmq
 */
routes.get('/', (req, res) => {
  rabbitmqConnection
    .createChannel()
    .then(ch => {
      ch.assertQueue('quetest')
        .then(() => {
          if (ch.sendToQueue('quetest', new Buffer('test'))) {
            res.status(200).send({ het: 'werkt' });
          } else {
            res.status(400).send({ error: 'unable to send to queue' });
          }
        })
        .catch(() => {
          res.status(400).send({ error: 'cannot assert queue' });
        });
    })
    .catch(() => {
      res.status(400).send({ error: 'please try again...' });
    });
});

function setRabbitMQConnection(connection) {
  rabbitmqConnection = connection;
}

module.exports = { routes, setRabbitMQConnection };
