const express = require('express');
const routes = express.Router();

const Template = require('../models/template.model');

/**
 * @swagger
 * /template:
 *    get:
 *     tags:
 *       - template (microservice mongo)
 *     description: This should return something templating retrieved from mongodb
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: something
 */
routes.get('/', (req, res) => {
  Template.find({})
    .then(temp => {
      res.status(200).send(temp);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

/**
 * @swagger
 * /template:
 *    post:
 *      tags:
 *        - template (microservice mongo)
 *      description: This should create an object in mongodb
 *      produces:
 *        - application/json
 *      responses:
 *        200:
 *          description: succesfully added
 */
routes.post('/', (req, res) => {
  const temp = new Template(req.body);

  temp
    .save()
    .then(() => {
      res.status(200).send(temp);
    })
    .catch(error => {
      res.status(400).send(error);
    });
});

module.exports = routes;
